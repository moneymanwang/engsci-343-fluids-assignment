import csv
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
import math
from scipy.optimize import curve_fit
import statistics 

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def  get_shortened_data(m30,m90, m120, m180, v30, v90, v120, v180):
    s_m30 = [0.02, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.12]
    s_v30 = []
    s_m90 = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.6, 0.7]
    s_v90 = []
    s_m120 = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.6]
    s_v120 = []
    s_m180 = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.6, 0.65, 0.7, 0.8]
    s_v180 = []

    for i in range(len(s_m30)):
        avg_v = []
        for j in range(len(m30)):
            if s_m30[i] == m30[j]:
                avg_v.append(v30[j])

        s_v30.append(statistics.mean(avg_v))

    for i in range(len(s_m90)):
        avg_v = []
        for j in range(len(m90)):
            if s_m90[i] == m90[j]:
                avg_v.append(v90[j])

        s_v90.append(statistics.mean(avg_v))

    for i in range(len(s_m120)):
        avg_v = []
        for j in range(len(m120)):
            if s_m120[i] == m120[j]:
                avg_v.append(v120[j])

        s_v120.append(statistics.mean(avg_v))

    for i in range(len(s_m180)):
        avg_v = []
        for j in range(len(m180)):
            if s_m180[i] == m180[j]:
                avg_v.append(v180[j])

        s_v180.append(statistics.mean(avg_v))

    return s_m30, s_m90, s_m120, s_m180, s_v30, s_v90, s_v120, s_v180

def plot_experimental_data(data30, data90, data120, data180):
    sns.regplot(x="Mass for 30 degrees", y="Velocity for 30 degrees", data=data30, label = "Deflection Angle of 30 degrees")
    sns.regplot(x="Mass for 90 degrees", y="Velocity for 90 degrees", data=data90, label = "Deflection Angle of 90 degrees")
    sns.regplot(x="Mass for 120 degrees", y="Velocity for 120 degrees", data=data120, label = "Deflection Angle of 120 degrees")
    sns.regplot(x="Mass for 180 degrees", y="Velocity for 180 degrees", data=data180, label = "Deflection Angle of 180 degrees")
    plt.legend()
    plt.title("Flow Velocity vs Balanced Mass")
    plt.xlabel("Balanced Mass in kg")
    plt.ylabel("Flow Velocity in m/s")
    plt.show()

def plot_theoretical_model_against_data(data30, data90, data120, data180):
    # Theoretical Model
    g = 9.81
    theta1 = (30*math.pi)/180
    theta2 = (90*math.pi)/180
    theta3 = (120*math.pi)/180
    theta4 = (180*math.pi)/180
    p = 997
    A = math.pi*(4*10**(-3))**2
    m30mod = np.linspace(0.02, 0.12, 100)
    m90mod = np.linspace(0.05, 0.7, 100)
    m120mod = np.linspace(0.05, 0.6, 100)
    m180mod = np.linspace(0.05, 0.8, 100)
    v30mod = np.sqrt((m30mod*g)/((1-math.cos(theta1))*p*A))
    v90mod = np.sqrt((m90mod*g)/((1-math.cos(theta2))*p*A))
    v120mod = np.sqrt((m120mod*g)/((1-math.cos(theta3))*p*A))
    v180mod = np.sqrt((m180mod*g)/((1-math.cos(theta4))*p*A))

    plt.plot(m30mod, v30mod, 'b', label = "Deflection Angle of 30 degrees (Model)")
    plt.plot(m90mod, v90mod, 'y', label = "Deflection Angle of 90 degrees (Model)")
    plt.plot(m120mod, v120mod, 'g', label = "Deflection Angle of 120 degrees (Model)")
    plt.plot(m180mod, v180mod, 'r', label = "Deflection Angle of 180 degrees (Model)")

    plt.scatter(x="Mass for 30 degrees", y="Velocity for 30 degrees", data=data30, label = "Deflection Angle of 30 degrees", c = 'b') 
    plt.scatter(x="Mass for 90 degrees", y="Velocity for 90 degrees", data=data90, label = "Deflection Angle of 90 degrees", c = 'y') 
    plt.scatter(x="Mass for 120 degrees", y="Velocity for 120 degrees", data=data120, label = "Deflection Angle of 120 degrees", c = 'g') 
    plt.scatter(x="Mass for 180 degrees", y="Velocity for 180 degrees", data=data180, label = "Deflection Angle of 180 degrees", c = 'r') 

    plt.title("Flow Velocity vs Balanced Mass")
    plt.xlabel("Balanced Mass in kg")
    plt.ylabel("Flow Velocity in m/s")
    plt.legend()
    plt.show()

def plot_residuals(data30, data90, data120, data180):
    g = 9.81
    theta1 = (30*math.pi)/180
    theta2 = (90*math.pi)/180
    theta3 = (120*math.pi)/180
    theta4 = (180*math.pi)/180
    p = 997
    A = math.pi*(4*10**(-3))**2
    v30mod = np.sqrt((data30['Mass for 30 degrees']*g)/((1-math.cos(theta1))*p*A))
    v90mod = np.sqrt((data90['Mass for 90 degrees']*g)/((1-math.cos(theta2))*p*A))
    v120mod = np.sqrt((data120['Mass for 120 degrees']*g)/((1-math.cos(theta3))*p*A))
    v180mod = np.sqrt((data180['Mass for 180 degrees']*g)/((1-math.cos(theta4))*p*A))

    sns.residplot(data30['Velocity for 30 degrees'], v30mod, lowess=True, color="b", label = "Deflection Angle of 30 degrees")
    sns.residplot(data90['Velocity for 90 degrees'], v90mod, lowess=True, color="y", label = "Deflection Angle of 90 degrees")
    sns.residplot(data120['Velocity for 120 degrees'], v120mod, lowess=True, color="g", label = "Deflection Angle of 120 degrees")
    sns.residplot(data180['Velocity for 180 degrees'], v180mod, lowess=True, color="r", label = "Deflection Angle of 180 degrees")
    plt.title("Residuals")
    plt.xlabel("Flow Velocity")
    plt.ylabel("Residuals")
    plt.legend()
    plt.show()

def flow_velocity_curve_fit_function_30(m, A):
    # Theoretical Model
    g = 9.81
    p = 997
    theta = (30*math.pi)/180
    v = np.sqrt((m*g)/((1-math.cos(theta))*p*A))
    return v

def flow_velocity_curve_fit_function_90(m, A):
    # Theoretical Model
    g = 9.81
    p = 997
    theta = (90*math.pi)/180
    v = np.sqrt((m*g)/((1-math.cos(theta))*p*A))
    return v

def flow_velocity_curve_fit_function_120(m, A):
    # Theoretical Model
    g = 9.81
    p = 997
    theta = (120*math.pi)/180
    v = np.sqrt((m*g)/((1-math.cos(theta))*p*A))
    return v

def flow_velocity_curve_fit_function_180(m, A):
    # Theoretical Model
    g = 9.81
    p = 997
    theta = (180*math.pi)/180
    v = np.sqrt((m*g)/((1-math.cos(theta))*p*A))
    return v

def calibrate_area(m_data, v_data, f):
    # Get optimal parameter values, using curve_fit
    pOpt, pCov = curve_fit(f, m_data, v_data, 5.0265*10**(-5))
    
    return pOpt, pCov

def plot_calibrated_model(m30,m90, m120, m180, v30, v90, v120, v180, r, data30, data90, data120, data180):
    optA30, _ = calibrate_area(m30, v30, flow_velocity_curve_fit_function_30)
    optA90, _ = calibrate_area(m90, v90, flow_velocity_curve_fit_function_90)
    optA120, _ = calibrate_area(m120, v120, flow_velocity_curve_fit_function_120)
    optA180, _ = calibrate_area(m180, v180, flow_velocity_curve_fit_function_180)

    # Theoretical Model
    g = 9.81
    theta1 = (30*math.pi)/180
    theta2 = (90*math.pi)/180
    theta3 = (120*math.pi)/180
    theta4 = (180*math.pi)/180
    p = 997
    m30mod = np.linspace(0.02, 0.12, 100)
    m90mod = np.linspace(0.05, 0.7, 100)
    m120mod = np.linspace(0.05, 0.6, 100)
    m180mod = np.linspace(0.05, 0.8, 100)

    v30mod = np.sqrt((m30mod*g)/((1-math.cos(theta1))*p*optA30))
    v90mod = np.sqrt((m90mod*g)/((1-math.cos(theta2))*p*optA90))
    v120mod = np.sqrt((m120mod*g)/((1-math.cos(theta3))*p*optA120))
    v180mod = np.sqrt((m180mod*g)/((1-math.cos(theta4))*p*optA180))

    plt.plot(m30mod, v30mod, 'b', label = "Deflection Angle of 30 degrees (Calibrated Model)")
    plt.plot(m90mod, v90mod, 'y', label = "Deflection Angle of 90 degrees (Calibrated Model)")
    plt.plot(m120mod, v120mod, 'g', label = "Deflection Angle of 120 degrees (Calibrated Model)")
    plt.plot(m180mod, v180mod, 'r', label = "Deflection Angle of 180 degrees (Calibrated Model)")

    plt.scatter(x="Mass for 30 degrees", y="Velocity for 30 degrees", data=data30, label = "Deflection Angle of 30 degrees", c = 'b')
    plt.scatter(x="Mass for 90 degrees", y="Velocity for 90 degrees", data=data90, label = "Deflection Angle of 90 degrees", c = 'y') 
    plt.scatter(x="Mass for 120 degrees", y="Velocity for 120 degrees", data=data120, label = "Deflection Angle of 120 degrees", c = 'g') 
    plt.scatter(x="Mass for 180 degrees", y="Velocity for 180 degrees", data=data180, label = "Deflection Angle of 180 degrees", c = 'r') 

    plt.title("Flow Velocity vs Balanced Mass")
    plt.xlabel("Balanced Mass in kg")
    plt.ylabel("Flow Velocity in m/s")
    plt.legend()
    plt.show()

    if r:
        A = math.pi*(4*10**(-3))**2
        v30mod = np.sqrt((data30['Mass for 30 degrees']*g)/((1-math.cos(theta1))*p*A))
        v30mod_c = np.sqrt((data30['Mass for 30 degrees']*g)/((1-math.cos(theta1))*p*optA30))
        v90mod_c = np.sqrt((data90['Mass for 90 degrees']*g)/((1-math.cos(theta2))*p*optA90))
        v120mod_c = np.sqrt((data120['Mass for 120 degrees']*g)/((1-math.cos(theta3))*p*optA120))
        v180mod_c = np.sqrt((data180['Mass for 180 degrees']*g)/((1-math.cos(theta4))*p*optA180))

        sns.residplot(data30['Velocity for 30 degrees'], v30mod_c, lowess=True, color="b", label = "Deflection Angle of 30 degrees")
        sns.residplot(data90['Velocity for 90 degrees'], v90mod_c, lowess=True, color="y", label = "Deflection Angle of 90 degrees")
        sns.residplot(data120['Velocity for 120 degrees'], v120mod_c, lowess=True, color="g", label = "Deflection Angle of 120 degrees")
        sns.residplot(data180['Velocity for 180 degrees'], v180mod_c, lowess=True, color="r", label = "Deflection Angle of 180 degrees")

        print(data30['Velocity for 30 degrees'])
        print()
        print(v30mod_c)
        print(v30mod)

        plt.title("Residuals")
        plt.xlabel("Flow Velocity")
        plt.ylabel("Residuals")
        plt.legend()
        plt.show()

if __name__ == "__main__":
    plot_exp_data = False
    plot_mod_vs_data = False
    plot_res = True
    plot_c_mod = True


    with open("data.txt", "r") as f:
        reader = csv.reader(f, delimiter="\t")
        d = list(reader)

    m30 = []
    m90 = []
    m120 = []
    m180 = []
    v30 = []
    v90 = []
    v120 = []
    v180 = []

    d = [list(filter(None, x)) for x in d]

    for ln in d:
        if ln == []:
            break
        if is_number(ln[0]):
            ln = [float(x) for x in ln]
            if ln[2] >= 150:
                m180.append(ln[0])
                v180.append(ln[1])
            if ln[2] < 150 and ln[2] >= 110:
                m120.append(ln[0])
                v120.append(ln[1])
            if ln[2] == 90:
                m90.append(ln[0])
                v90.append(ln[1])
            if ln[2] < 50:
                m30.append(ln[0])
                v30.append(ln[1])

    m30.sort()
    m90.sort()
    m120.sort()
    m180.sort()
    v30.sort()
    v90.sort()
    v120.sort()
    v180.sort()

    s_m30, s_m90, s_m120, s_m180, s_v30, s_v90, s_v120, s_v180 = get_shortened_data(m30,m90, m120, m180, v30, v90, v120, v180)

    s_data30 = pd.DataFrame({'Mass for 30 degrees' : s_m30, 'Velocity for 30 degrees' : s_v30})
    s_data90 = pd.DataFrame({"Mass for 90 degrees" : s_m90, "Velocity for 90 degrees" : s_v90})
    s_data120 = pd.DataFrame({"Mass for 120 degrees" : s_m120, "Velocity for 120 degrees" : s_v120})
    s_data180 = pd.DataFrame({"Mass for 180 degrees" : s_m180, "Velocity for 180 degrees" : s_v180})

    data30 = pd.DataFrame({'Mass for 30 degrees' : m30, 'Velocity for 30 degrees' : v30})
    data90 = pd.DataFrame({"Mass for 90 degrees" : m90, "Velocity for 90 degrees" : v90})
    data120 = pd.DataFrame({"Mass for 120 degrees" : m120, "Velocity for 120 degrees" : v120})
    data180 = pd.DataFrame({"Mass for 180 degrees" : m180, "Velocity for 180 degrees" : v180})

    if plot_exp_data:
        plot_experimental_data(data30, data90, data120, data180)

    if plot_mod_vs_data:
        plot_theoretical_model_against_data(data30, data90, data120, data180)

    if plot_res:
        plot_residuals(s_data30, s_data90, s_data120, s_data180)

    if plot_c_mod:
        plot_calibrated_model(s_m30,s_m90, s_m120, s_m180, s_v30, s_v90, s_v120, s_v180, True, s_data30, s_data90, s_data120, s_data180)



    






